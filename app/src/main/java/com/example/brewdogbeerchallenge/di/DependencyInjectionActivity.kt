package com.example.brewdogbeerchallenge.di

import androidx.appcompat.app.AppCompatActivity
import com.example.api.common.fastLazy
import com.example.brewdogbeerchallenge.common.AppApplication

abstract class DependencyInjectionActivity : AppCompatActivity() {

    val presentationComponent: PresentationComponent by fastLazy {
        (application as AppApplication).appComponent.presentationComponent(
            PresentationModule(this)
        )
    }
}
