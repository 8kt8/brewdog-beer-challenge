package com.example.brewdogbeerchallenge.di

import com.example.brewdogbeerchallenge.ui.beerDetails.FragmentBeerDetails
import com.example.brewdogbeerchallenge.ui.beerDetails.di.BeerDetailsModule
import com.example.brewdogbeerchallenge.ui.listbeer.FragmentListBeer
import com.example.brewdogbeerchallenge.ui.listbeer.di.ListBeerModule
import com.example.brewdogbeerchallenge.ui.main.BaseFragment
import com.example.brewdogbeerchallenge.ui.main.MainActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(
    modules = [
        PresentationModule::class,
        ViewModelModule::class,
        ListBeerModule::class,
        BeerDetailsModule::class
    ]
)
interface PresentationComponent {

    fun inject(activity: MainActivity)

    fun inject(baseFragment: BaseFragment)

    fun inject(fragmentListBeer: FragmentListBeer)

    fun inject(fragmentBeerDetails: FragmentBeerDetails)
}
