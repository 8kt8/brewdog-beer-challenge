package com.example.brewdogbeerchallenge.ui.listbeer.uiModel

import com.example.api.model.domain.beer.Beer
import com.example.brewdogbeerchallenge.ui.abv.AbvUiModelMapper
import javax.inject.Inject

class BeerItemUiModelMapper @Inject constructor(
    private val abvUiModelMapper: AbvUiModelMapper
) {

    fun toBeerItemUiModels(beer: List<Beer>): List<BeerItemUiModel> = beer.map(::toBeerItemUiModel)

    fun toBeerItemUiModel(beer: Beer): BeerItemUiModel = with(beer){
            BeerItemUiModel(
                id = id,
                name = name,
                imageUrl = imageUrl,
                abv = abvUiModelMapper.toUiModel(abv)
            )
    }
}