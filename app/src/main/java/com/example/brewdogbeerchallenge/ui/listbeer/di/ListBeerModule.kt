package com.example.brewdogbeerchallenge.ui.listbeer.di

import androidx.lifecycle.ViewModel
import com.example.brewdogbeerchallenge.di.ViewModelKey
import com.example.brewdogbeerchallenge.ui.listbeer.ListBeerViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ListBeerModule {
    @Binds
    @IntoMap
    @ViewModelKey(ListBeerViewModel::class)
    internal abstract fun bindListBeerViewModel(userViewModel: ListBeerViewModel): ViewModel

}