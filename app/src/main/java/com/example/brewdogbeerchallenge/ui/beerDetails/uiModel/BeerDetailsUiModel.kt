package com.example.brewdogbeerchallenge.ui.beerDetails.uiModel

import android.text.Spannable

data class BeerDetailsUiModel(
    val id: Int,
    val name: String,
    val imageUrl: String,
    val abv: String,
    val description: String,
    val hops: Spannable,
    val malts: Spannable,
    val method: Spannable
)