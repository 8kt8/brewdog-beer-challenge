package com.example.brewdogbeerchallenge.ui.listbeer

import androidx.lifecycle.LiveDataReactiveStreams.fromPublisher
import com.example.api.repository.interactor.GetBeerInteractor
import com.example.api.repository.interactor.RefreshBeerInteractor
import com.example.brewdogbeerchallenge.ui.listbeer.uiModel.BeerItemUiModelMapper
import com.example.brewdogbeerchallenge.ui.main.BaseViewModel
import com.orhanobut.logger.Logger
import javax.inject.Inject

class ListBeerViewModel @Inject constructor(
    private val getBeerInteractor: GetBeerInteractor,
    private val refreshBeerInteractor: RefreshBeerInteractor,
    private val bearUiModelMapper: BeerItemUiModelMapper
): BaseViewModel() {

    fun getAllBeer() = fromPublisher(
        getBeerInteractor.getAll()
            .distinctUntilChanged()
            .map(bearUiModelMapper::toBeerItemUiModels)
    )

    fun refreshData() {
        refreshBeerInteractor.refresh()
            .subscribe({}, {
                Logger.e(it.localizedMessage ?: "")
            })
            .remember()
    }

}