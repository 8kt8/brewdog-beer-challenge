package com.example.brewdogbeerchallenge.ui.beerDetails.uiComponent

import com.example.brewdogbeerchallenge.databinding.FragmentBeerDetailsBinding
import com.example.brewdogbeerchallenge.ui.beerDetails.BeerDetailsViewModel
import com.example.brewdogbeerchallenge.ui.beerDetails.FragmentBeerDetailsArgs
import com.example.brewdogbeerchallenge.ui.uiComponent.UiComponentHost

interface FragmentBeerDetailsUiComponentHost: UiComponentHost {

    val viewModel: BeerDetailsViewModel
    val binding: FragmentBeerDetailsBinding
    val args: FragmentBeerDetailsArgs
}