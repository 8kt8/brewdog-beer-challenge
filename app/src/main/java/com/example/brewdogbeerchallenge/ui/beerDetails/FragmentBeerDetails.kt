package com.example.brewdogbeerchallenge.ui.beerDetails

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.example.api.common.fastLazy
import com.example.brewdogbeerchallenge.R
import com.example.brewdogbeerchallenge.common.getPresentationComponent
import com.example.brewdogbeerchallenge.databinding.FragmentBeerDetailsBinding
import com.example.brewdogbeerchallenge.ui.beerDetails.uiComponent.FragmentBeerDetailsUiComponent
import com.example.brewdogbeerchallenge.ui.beerDetails.uiComponent.FragmentBeerDetailsUiComponentHost
import com.example.brewdogbeerchallenge.ui.main.BindingFragment
import javax.inject.Inject

class FragmentBeerDetails: BindingFragment<FragmentBeerDetailsBinding>(),
    FragmentBeerDetailsUiComponentHost {

    @Inject
    lateinit var uiComponent: FragmentBeerDetailsUiComponent

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getPresentationComponent().inject(this)
    }

    override val binding: FragmentBeerDetailsBinding
        get() = _binding

    override val args: FragmentBeerDetailsArgs by navArgs()

    override val viewModel by fastLazy { forFragment<BeerDetailsViewModel>() }

    override val layoutId: Int = R.layout.fragment_beer_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uiComponent.init(this)
    }
}