package com.example.brewdogbeerchallenge.ui.beerDetails

import androidx.lifecycle.LiveDataReactiveStreams.fromPublisher
import com.example.api.repository.interactor.GetBeerByIdInteractor
import com.example.brewdogbeerchallenge.ui.beerDetails.uiModel.BeerDetailsUiModelMapper
import com.example.brewdogbeerchallenge.ui.main.BaseViewModel
import javax.inject.Inject

class BeerDetailsViewModel @Inject constructor(
    private val getBeerByIdInteractor: GetBeerByIdInteractor,
    private val beerDetailsUiModelMapper: BeerDetailsUiModelMapper
): BaseViewModel() {

    fun getBeer(id: Int) = fromPublisher(
        getBeerByIdInteractor.getById(id)
            .distinctUntilChanged()
            .map(beerDetailsUiModelMapper::toUiModel)
    )
}