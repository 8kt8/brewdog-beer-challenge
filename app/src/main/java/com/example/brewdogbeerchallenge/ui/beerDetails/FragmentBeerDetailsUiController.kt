package com.example.brewdogbeerchallenge.ui.beerDetails

import com.example.brewdogbeerchallenge.ui.beerDetails.uiComponent.FragmentBeerDetailsUiComponent
import com.example.brewdogbeerchallenge.ui.beerDetails.uiModel.BeerDetailsUiModel
import com.example.brewdogbeerchallenge.ui.uiController.UiController
import javax.inject.Inject

class FragmentBeerDetailsUiController @Inject constructor(): UiController<FragmentBeerDetailsUiComponent>() {

    override fun init(uiComponent: FragmentBeerDetailsUiComponent) {
        super.init(uiComponent)
        uiComponent.showLoading()
    }

    fun onBoundBeerItem(beerUiModel: BeerDetailsUiModel){
        uiComponent.hideLoading()
        uiComponent.setData(beerUiModel)
    }
}