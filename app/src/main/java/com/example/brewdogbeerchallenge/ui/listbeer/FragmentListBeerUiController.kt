package com.example.brewdogbeerchallenge.ui.listbeer

import com.example.brewdogbeerchallenge.ui.listbeer.uiComponent.FragmentListBeerUiComponent
import com.example.brewdogbeerchallenge.ui.listbeer.uiModel.BeerItemUiModel
import com.example.brewdogbeerchallenge.ui.uiController.UiController
import javax.inject.Inject

class FragmentListBeerUiController @Inject constructor(): UiController<FragmentListBeerUiComponent>() {

    override fun init(uiComponent: FragmentListBeerUiComponent) {
        super.init(uiComponent)
        uiComponent.showLoading()
    }

    fun onBoundBeerItems(beerUiModels: List<BeerItemUiModel>){
        uiComponent.hideLoading()
        uiComponent.setNewListData(beerUiModels)
    }

    fun onBoundBeerItemClick(beerUiModel: BeerItemUiModel){
        val navDirections = FragmentListBeerDirections.actionBeerListToDetails(beerUiModel.id)
        uiComponent.host.navigateToBeerDetails(navDirections)

    }
}