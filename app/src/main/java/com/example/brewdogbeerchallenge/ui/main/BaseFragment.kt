package com.example.brewdogbeerchallenge.ui.main

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import com.example.brewdogbeerchallenge.ui.uiComponent.UiComponentHost
import javax.inject.Inject

abstract class BaseFragment: Fragment(), UiComponentHost {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val lifecycleOwnerOfView: LifecycleOwner
        get() = viewLifecycleOwner

}