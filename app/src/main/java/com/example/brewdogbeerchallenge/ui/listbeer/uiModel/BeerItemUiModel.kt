package com.example.brewdogbeerchallenge.ui.listbeer.uiModel

data class BeerItemUiModel(
    val id: Int,
    val name: String,
    val imageUrl: String,
    val abv: String
)