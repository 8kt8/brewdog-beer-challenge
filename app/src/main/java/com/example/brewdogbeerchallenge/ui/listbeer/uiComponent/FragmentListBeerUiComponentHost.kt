package com.example.brewdogbeerchallenge.ui.listbeer.uiComponent

import androidx.navigation.NavDirections
import com.example.brewdogbeerchallenge.databinding.FragmentListBeerBinding
import com.example.brewdogbeerchallenge.ui.listbeer.ListBeerViewModel
import com.example.brewdogbeerchallenge.ui.uiComponent.UiComponentHost

interface FragmentListBeerUiComponentHost: UiComponentHost {

    val viewModel: ListBeerViewModel
    val binding: FragmentListBeerBinding
    fun navigateToBeerDetails(navDirections: NavDirections)
}