package com.example.brewdogbeerchallenge.ui.uiComponent

import androidx.lifecycle.LifecycleOwner

interface UiComponentHost {

    val lifecycleOwnerOfView: LifecycleOwner
}
