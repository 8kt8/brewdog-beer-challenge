package com.example.brewdogbeerchallenge.ui.abv

import javax.inject.Inject

class AbvUiModelMapper @Inject constructor(){

    fun toUiModel(abv: Double) = "Abv $abv%"
}