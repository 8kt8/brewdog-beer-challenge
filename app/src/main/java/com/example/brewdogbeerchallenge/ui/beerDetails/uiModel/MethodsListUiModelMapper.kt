package com.example.brewdogbeerchallenge.ui.beerDetails.uiModel

import android.text.Spannable
import com.example.api.model.domain.method.Method
import com.example.brewdogbeerchallenge.ui.title.SpannableTitleFactory
import javax.inject.Inject

class MethodsListUiModelMapper @Inject constructor(
    private val spannableTitleFactory: SpannableTitleFactory
){

    fun toUiModel(method: Method?): Spannable {
        if(method == null){
            return spannableTitleFactory.create("Method: -\n")
        }

        val ssb = spannableTitleFactory.create("Method: \n")
        ssb.append("Mash temp: \n")

        method.mashTemp.forEach {
            ssb.append("\u2022 ${it.temp.value} ${it.temp.unit}\n")
            ssb.append("Duration: ${it.duration ?: "-"} \n\n")
        }
        ssb.append("Fermentation: \n")
        ssb.append("${method.fermentation.temp.value} ${method.fermentation.temp.unit} \n")
        ssb.append("\nTwist ${method.twist ?: "-"}\n")
        return ssb.delete(ssb.length - 3, ssb.length - 2)
    }

}