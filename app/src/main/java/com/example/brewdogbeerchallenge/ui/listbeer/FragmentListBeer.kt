package com.example.brewdogbeerchallenge.ui.listbeer

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.api.common.fastLazy
import com.example.brewdogbeerchallenge.R
import com.example.brewdogbeerchallenge.common.getPresentationComponent
import com.example.brewdogbeerchallenge.databinding.FragmentListBeerBinding
import com.example.brewdogbeerchallenge.ui.listbeer.uiComponent.FragmentListBeerUiComponent
import com.example.brewdogbeerchallenge.ui.listbeer.uiComponent.FragmentListBeerUiComponentHost
import com.example.brewdogbeerchallenge.ui.main.BindingFragment
import javax.inject.Inject

class FragmentListBeer : BindingFragment<FragmentListBeerBinding>(),
    FragmentListBeerUiComponentHost {

    @Inject
    lateinit var uiComponent: FragmentListBeerUiComponent

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getPresentationComponent().inject(this)
    }

    override val binding: FragmentListBeerBinding
        get() = _binding

    override val viewModel by fastLazy { forFragment<ListBeerViewModel>() }

    override val layoutId: Int = R.layout.fragment_list_beer

    override fun navigateToBeerDetails(navDirections: NavDirections) {
        findNavController().navigate(navDirections)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uiComponent.init(this)
    }

    override fun onResume() {
        super.onResume()
        viewModel.refreshData()
    }
}