package com.example.brewdogbeerchallenge.ui.beerDetails.uiModel

import com.example.api.model.domain.beer.Beer
import com.example.brewdogbeerchallenge.ui.abv.AbvUiModelMapper
import javax.inject.Inject

class BeerDetailsUiModelMapper @Inject constructor(
    private val abvUiModelMapper: AbvUiModelMapper,
    private val hopsListUiModelMapper: HoopsListUiModelMapper,
    private val maltsListUiModelMapper: MaltsListUiModelMapper,
    private val methodsListUiModelMapper: MethodsListUiModelMapper
) {

    fun toUiModel(beer: Beer) = with(beer){
        BeerDetailsUiModel(
            id = id,
            imageUrl = imageUrl,
            name = name,
            abv = abvUiModelMapper.toUiModel(abv),
            description = description,
            method = methodsListUiModelMapper.toUiModel(method),
            malts = maltsListUiModelMapper.toUiModel(ingredients?.malt),
            hops = hopsListUiModelMapper.toUiModel(ingredients?.hops)
        )
    }
}