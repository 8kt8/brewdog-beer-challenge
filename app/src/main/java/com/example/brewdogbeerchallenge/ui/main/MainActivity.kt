package com.example.brewdogbeerchallenge.ui.main

import android.os.Bundle
import com.example.api.repository.interactor.GetBeerInteractor
import com.example.api.repository.interactor.RefreshBeerInteractor
import com.example.api.rx.CoreSchedulers
import com.example.brewdogbeerchallenge.R
import com.example.brewdogbeerchallenge.di.DependencyInjectionActivity
import javax.inject.Inject

class MainActivity : DependencyInjectionActivity() {

    @Inject
    lateinit var getBeerInteractor: GetBeerInteractor

    @Inject
    lateinit var refreshBeerInteractor: RefreshBeerInteractor

    @Inject
    lateinit var coreSchedulers: CoreSchedulers

    override fun onCreate(savedInstanceState: Bundle?) {
        presentationComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}