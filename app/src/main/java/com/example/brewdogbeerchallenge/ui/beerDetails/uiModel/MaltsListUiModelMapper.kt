package com.example.brewdogbeerchallenge.ui.beerDetails.uiModel

import android.text.Spannable
import com.example.api.model.domain.malt.Malt
import com.example.brewdogbeerchallenge.ui.title.SpannableTitleFactory
import javax.inject.Inject

class MaltsListUiModelMapper @Inject constructor(
    private val spannableTitleFactory: SpannableTitleFactory
){

    fun toUiModel(hops: List<Malt>?): Spannable {
        if(hops == null){
            return spannableTitleFactory.create("Malts: -\n")
        }
        val ssb =spannableTitleFactory.create("Malts: \n")
        hops.forEach {
            ssb.append("\u2022 ${it.name}:\n")
            ssb.append("${it.amount.value} ${it.amount.unit}\n\n")
        }
        return ssb.delete(ssb.length - 3, ssb.length - 2)
    }

}