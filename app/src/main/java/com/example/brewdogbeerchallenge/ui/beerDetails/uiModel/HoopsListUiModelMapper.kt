package com.example.brewdogbeerchallenge.ui.beerDetails.uiModel

import android.text.Spannable
import com.example.api.model.domain.hops.Hops
import com.example.brewdogbeerchallenge.ui.title.SpannableTitleFactory
import javax.inject.Inject


class HoopsListUiModelMapper @Inject constructor(
    private val spannableTitleFactory: SpannableTitleFactory
){

    fun toUiModel(hops: List<Hops>?): Spannable {
        if(hops == null){
            return spannableTitleFactory.create("Hops:- \n")
        }

        val ssb = spannableTitleFactory.create("Hops:\n")
        hops.forEach {
            ssb.append("\u2022 ${it.name}:\n")
            ssb.append("${it.amount.value} ${it.amount.unit}\n")
            ssb.append("${getAddString(it.add)}\n")
            ssb.append("Attribute: ${it.attribute}\n\n")
        }

        return ssb.delete(ssb.length - 3, ssb.length - 2)
    }

    private fun getAddString(add: String) = when{
            add.contains("start") -> "Add on beginning"
            add.contains("middle") -> "Add in the middle"
            add.contains("end") -> "Add on the end"
            else -> ""
    }
}