package com.example.brewdogbeerchallenge.ui.listbeer.uiComponent

import android.view.View
import androidx.lifecycle.observe
import com.example.brewdogbeerchallenge.ui.listbeer.FragmentListBeerUiController
import com.example.brewdogbeerchallenge.ui.listbeer.uiModel.BeerItemUiModel
import com.example.brewdogbeerchallenge.ui.uiComponent.UiComponent
import javax.inject.Inject

class FragmentListBeerUiComponent @Inject constructor(
    override val uiController: FragmentListBeerUiController,
    private val listBeerUiComponent: ListBeerUiComponent
): UiComponent<FragmentListBeerUiComponentHost>() {

    override fun initUiController() = uiController.init(this)

    override fun init(host: FragmentListBeerUiComponentHost) {
        super.init(host)
        observeBeer()
        initListBeerComponent()
    }

    fun setNewListData(beerUiModels: List<BeerItemUiModel>) = listBeerUiComponent.setData(beerUiModels)

    fun showLoading(){
        host.binding.apply {
            recyclerView.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }
    }

    fun hideLoading(){
        host.binding.apply {
            recyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

    private fun initListBeerComponent() {
        listBeerUiComponent.apply {
            bindView(host.binding.recyclerView)
            onClickAction = uiController::onBoundBeerItemClick
        }
    }

    private fun observeBeer(){
        host.viewModel.getAllBeer()
            .observe(host.lifecycleOwnerOfView){
                uiController.onBoundBeerItems(it)
            }

    }
}