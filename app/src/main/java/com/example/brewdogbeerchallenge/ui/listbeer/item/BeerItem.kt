package com.example.brewdogbeerchallenge.ui.listbeer.item

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.brewdogbeerchallenge.R
import com.example.brewdogbeerchallenge.databinding.ItemListBeerBinding
import com.example.brewdogbeerchallenge.ui.listbeer.uiModel.BeerItemUiModel
import com.mikepenz.fastadapter.binding.AbstractBindingItem

data class BeerItem(val uiModel: BeerItemUiModel): AbstractBindingItem<ItemListBeerBinding>() {

    override val type: Int = R.id.beer_list_item

    override fun bindView(binding: ItemListBeerBinding, payloads: List<Any>) {
        binding.uiModel = uiModel
        Glide
            .with(binding.root.context)
            .load(uiModel.imageUrl)
            .fitCenter()
            .placeholder(android.R.drawable.ic_dialog_info)
            .into(binding.imageView)
    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemListBeerBinding =
        ItemListBeerBinding.inflate(inflater, parent, false)

}