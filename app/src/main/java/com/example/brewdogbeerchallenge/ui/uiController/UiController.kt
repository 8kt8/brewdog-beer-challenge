package com.example.brewdogbeerchallenge.ui.uiController

import com.example.brewdogbeerchallenge.ui.uiComponent.UiComponent

abstract class UiController<Component : UiComponent<*>> {

    protected lateinit var uiComponent: Component
        private set

    open fun init(uiComponent: Component) {
        this.uiComponent = uiComponent
    }
}
