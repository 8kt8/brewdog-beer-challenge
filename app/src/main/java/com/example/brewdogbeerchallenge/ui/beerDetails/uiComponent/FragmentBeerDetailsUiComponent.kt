package com.example.brewdogbeerchallenge.ui.beerDetails.uiComponent

import android.view.View
import androidx.lifecycle.observe
import com.bumptech.glide.Glide
import com.example.brewdogbeerchallenge.ui.beerDetails.FragmentBeerDetailsUiController
import com.example.brewdogbeerchallenge.ui.beerDetails.uiModel.BeerDetailsUiModel
import com.example.brewdogbeerchallenge.ui.uiComponent.UiComponent
import javax.inject.Inject


class FragmentBeerDetailsUiComponent @Inject constructor(
    override val uiController: FragmentBeerDetailsUiController
) : UiComponent<FragmentBeerDetailsUiComponentHost>() {

    override fun initUiController() = uiController.init(this)

    override fun init(host: FragmentBeerDetailsUiComponentHost) {
        super.init(host)
        observeBeer()
    }

    fun showLoading() {
        host.binding.apply {
            scrollLayout.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }
    }

    fun hideLoading() {
        host.binding.apply {
            scrollLayout.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

    fun setData(uiModel: BeerDetailsUiModel) {
        host.binding.apply {
            textViewName.text = uiModel.name
            textViewAbv.text = uiModel.abv
            textViewDescription.text = uiModel.description
            textViewHopsList.text = uiModel.hops
            textViewMaltsList.text = uiModel.malts
            textViewMethodList.text = uiModel.method

            Glide
                .with(root.context)
                .load(uiModel.imageUrl)
                .optionalFitCenter()
                .placeholder(android.R.drawable.ic_dialog_info)
                .into(imageView)
        }
    }


    private fun observeBeer() {
        with(host) {
            viewModel.getBeer(args.beerId)
                .observe(lifecycleOwnerOfView) {
                    uiController.onBoundBeerItem(it)
                }
        }
    }
}