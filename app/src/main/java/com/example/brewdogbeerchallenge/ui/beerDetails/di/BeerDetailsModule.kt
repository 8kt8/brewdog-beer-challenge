package com.example.brewdogbeerchallenge.ui.beerDetails.di

import androidx.lifecycle.ViewModel
import com.example.brewdogbeerchallenge.di.ViewModelKey
import com.example.brewdogbeerchallenge.ui.beerDetails.BeerDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class BeerDetailsModule {
    @Binds
    @IntoMap
    @ViewModelKey(BeerDetailsViewModel::class)
    internal abstract fun bindBeerDetailsViewModel(userViewModel: BeerDetailsViewModel): ViewModel

}