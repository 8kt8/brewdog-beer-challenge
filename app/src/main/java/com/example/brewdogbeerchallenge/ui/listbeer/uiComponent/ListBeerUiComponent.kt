package com.example.brewdogbeerchallenge.ui.listbeer.uiComponent

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.brewdogbeerchallenge.ui.listbeer.item.BeerItem
import com.example.brewdogbeerchallenge.ui.listbeer.item.BeerItemMapper
import com.example.brewdogbeerchallenge.ui.listbeer.uiModel.BeerItemUiModel
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import javax.inject.Inject

class ListBeerUiComponent @Inject constructor(
    private val beerItemMapper: BeerItemMapper
) {

    private lateinit var _recyclerView: RecyclerView

    private val fastItemAdapter = FastItemAdapter<BeerItem>()

    var onClickAction: (BeerItemUiModel) -> Unit = {}

    fun bindView(recyclerView: RecyclerView) {
        _recyclerView = recyclerView.apply {
            adapter = fastItemAdapter
            layoutManager = LinearLayoutManager(context)
        }
        fastItemAdapter.onClickListener = { _, _, item, _->
            onClickAction(item.uiModel)
            false
        }
    }

    fun setData(beer: List<BeerItemUiModel>){
        val newItems = beerItemMapper.toBeer(beer)
        FastAdapterDiffUtil[fastItemAdapter.itemAdapter] = newItems
    }
}