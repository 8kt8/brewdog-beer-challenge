package com.example.brewdogbeerchallenge.ui.listbeer.item

import com.example.brewdogbeerchallenge.ui.listbeer.uiModel.BeerItemUiModel
import javax.inject.Inject

class BeerItemMapper @Inject constructor() {

    fun toBeer(beer: List<BeerItemUiModel>): List<BeerItem> = beer.map(::toBeer)

    fun toBeer(uiModel: BeerItemUiModel): BeerItem = BeerItem(uiModel).apply {
        identifier = uiModel.id.toLong()
    }
}