package com.example.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MashTempResponse (
	@Json(name = "temp") val temp : TempResponse,
	@Json(name = "duration") val duration : String?
)