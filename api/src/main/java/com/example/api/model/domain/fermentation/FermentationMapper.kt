package com.example.api.model.domain.fermentation

import com.example.api.model.FermentationResponse
import com.example.api.model.domain.temp.TempMapper
import javax.inject.Inject

class FermentationMapper @Inject constructor(
    private val tempMapper: TempMapper
) {

    fun toFermentation(fermentationResponse: FermentationResponse): Fermentation =
        Fermentation(tempMapper.toTemp(fermentationResponse.temp))

}