package com.example.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BeerResponse(
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String,
    @Json(name = "description") val description: String,
    @Json(name = "image_url") val imageUrl: String,
    @Json(name = "abv") val abv: Double,
    @Json(name = "method") val method: MethodResponse,
    @Json(name = "ingredients") val ingredients: IngredientsResponse
)