package com.example.api.model.domain.beer

import com.example.api.model.BeerResponse
import com.example.api.model.domain.ingredients.IngredientsMapper
import com.example.api.model.domain.method.MethodMapper
import javax.inject.Inject

class BeerMapper @Inject constructor(
    private val methodMapper: MethodMapper,
    private val ingredientsMapper: IngredientsMapper
) {
    fun toBeer(beer: List<BeerResponse>): List<Beer> = beer.map(::toBeer)

    private fun toBeer(beer: BeerResponse): Beer = with(beer){
        Beer(
            id = id,
            name = name,
            description = description,
            imageUrl = imageUrl,
            abv = abv,
            method = methodMapper.toMethod(method),
            ingredients = ingredientsMapper.toIngredients(ingredients)
        )
    }
}