package com.example.api.model.domain.malt

import com.example.api.model.MaltResponse
import com.example.api.model.domain.amount.AmountMapper
import javax.inject.Inject

class MaltMapper @Inject constructor(
    private val amountMapper: AmountMapper
) {

    fun toMalt(maltResponse: List<MaltResponse>): List<Malt> = maltResponse.map(::toMalt)

    private fun toMalt(maltResponse: MaltResponse): Malt = with(maltResponse){
        Malt(
            name = name,
            amount = amountMapper.toAmount(amount))
    }
}