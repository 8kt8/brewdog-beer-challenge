package com.example.api.model.domain.fermentation

import com.example.api.model.domain.temp.Temp

data class Fermentation(
    val temp: Temp
)