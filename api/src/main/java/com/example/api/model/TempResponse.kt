package com.example.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TempResponse (
	@Json(name = "value") val value: Int,
	@Json(name = "unit") val unit: String
)