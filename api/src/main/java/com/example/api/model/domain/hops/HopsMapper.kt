package com.example.api.model.domain.hops

import com.example.api.model.HopsResponse
import com.example.api.model.domain.amount.AmountMapper
import javax.inject.Inject

class HopsMapper @Inject constructor(
    private val amountMapper: AmountMapper
){

    fun toHops(hopsResponse: List<HopsResponse>): List<Hops> = hopsResponse.map(::toHops)

    private fun toHops(hopsResponse: HopsResponse): Hops = with(hopsResponse){
        Hops(
            name = name,
            amount = amountMapper.toAmount(amount),
            add = add,
            attribute = attribute
        )
    }
}