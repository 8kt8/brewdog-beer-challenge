package com.example.api.model.domain.ingredients

import com.example.api.model.domain.hops.Hops
import com.example.api.model.domain.malt.Malt

data class Ingredients (
	val malt : List<Malt>,
	val hops : List<Hops>
)