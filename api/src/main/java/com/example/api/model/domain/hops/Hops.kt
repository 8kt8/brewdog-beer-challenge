package com.example.api.model.domain.hops

import com.example.api.model.domain.amount.Amount

data class Hops(
    val name: String,
    val amount: Amount,
    val add: String,
    val attribute: String
)