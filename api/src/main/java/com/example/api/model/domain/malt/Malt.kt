package com.example.api.model.domain.malt

import com.example.api.model.domain.amount.Amount

data class Malt(
    val name: String,
    val amount: Amount
)