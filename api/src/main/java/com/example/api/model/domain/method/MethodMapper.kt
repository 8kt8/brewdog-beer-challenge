package com.example.api.model.domain.method

import com.example.api.model.MethodResponse
import com.example.api.model.domain.fermentation.FermentationMapper
import com.example.api.model.domain.mashTemp.MashTempMapper
import javax.inject.Inject

class MethodMapper @Inject constructor(
    private val mashTempMapper: MashTempMapper,
    private val fermentationMapper: FermentationMapper
) {

    fun toMethod(method: MethodResponse): Method = with(method){
        Method(
            mashTemp = mashTempMapper.toMashTemp(mashTemp),
            fermentation = fermentationMapper.toFermentation(fermentation),
            twist = twist
        )
    }
}