package com.example.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MethodResponse(
    @Json(name = "mash_temp") val mashTemp : List<MashTempResponse>,
    @Json(name = "fermentation") val fermentation : FermentationResponse,
    @Json(name = "twist") val twist : String?
)