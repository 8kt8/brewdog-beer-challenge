package com.example.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class FermentationResponse(
    @Json(name = "temp") val temp: TempResponse
)