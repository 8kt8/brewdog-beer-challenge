package com.example.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HopsResponse(
    @Json(name = "name") val name: String,
    @Json(name = "amount") val amount: AmountResponse,
    @Json(name = "add") val add: String,
    @Json(name = "attribute") val attribute: String
)