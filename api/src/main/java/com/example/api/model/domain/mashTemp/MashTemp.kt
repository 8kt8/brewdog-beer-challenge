package com.example.api.model.domain.mashTemp

import com.example.api.model.domain.temp.Temp

data class MashTemp (
	val temp : Temp,
	val duration : String?
)