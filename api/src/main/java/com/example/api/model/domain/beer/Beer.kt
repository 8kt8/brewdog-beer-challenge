package com.example.api.model.domain.beer

import com.example.api.model.domain.ingredients.Ingredients
import com.example.api.model.domain.method.Method

data class Beer(
    val id: Int,
    val name: String,
    val description: String,
    val imageUrl: String,
    val abv: Double,
    val method: Method?,
    val ingredients: Ingredients?
)