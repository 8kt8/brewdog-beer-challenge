package com.example.api.model.domain.amount

import com.example.api.model.AmountResponse
import javax.inject.Inject

class AmountMapper @Inject constructor() {

    fun toAmount(amountResponse: AmountResponse) = with(amountResponse){
        Amount(value, unit)
    }
}