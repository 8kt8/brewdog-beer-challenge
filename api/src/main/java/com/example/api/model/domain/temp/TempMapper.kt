package com.example.api.model.domain.temp

import com.example.api.model.TempResponse
import javax.inject.Inject

class TempMapper @Inject constructor() {

    fun toTemp(tempResponse: TempResponse) = with(tempResponse){
        Temp(value, unit)
    }
}