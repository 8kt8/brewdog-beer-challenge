package com.example.api.model.domain.method

import com.example.api.model.domain.fermentation.Fermentation
import com.example.api.model.domain.mashTemp.MashTemp

data class Method(
    val mashTemp : List<MashTemp>,
    val fermentation : Fermentation,
    val twist : String?
)