package com.example.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AmountResponse(
    @Json(name = "value") val value: Float,
    @Json(name = "unit") val unit: String
)