package com.example.api.model.domain.amount

data class Amount(
   val value: Float,
   val unit: String
)