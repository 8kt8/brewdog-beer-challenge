package com.example.api.model.domain.temp

data class Temp (
	val value: Int,
	val unit: String
)