package com.example.api.model.domain.ingredients

import com.example.api.model.IngredientsResponse
import com.example.api.model.domain.hops.HopsMapper
import com.example.api.model.domain.malt.MaltMapper
import javax.inject.Inject

class IngredientsMapper @Inject constructor(
    private val maltMapper: MaltMapper,
    private val hopsMapper: HopsMapper
) {

    fun toIngredients(ingredientsResponse: IngredientsResponse) = with(ingredientsResponse){
        Ingredients(
            malt = maltMapper.toMalt(malt),
            hops = hopsMapper.toHops(hops)
        )
    }
}