package com.example.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class IngredientsResponse (
	@Json(name = "malt") val malt : List<MaltResponse>,
	@Json(name = "hops") val hops : List<HopsResponse>
)