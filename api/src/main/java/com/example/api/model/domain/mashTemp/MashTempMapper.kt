package com.example.api.model.domain.mashTemp

import com.example.api.model.MashTempResponse
import com.example.api.model.domain.temp.TempMapper
import javax.inject.Inject

class MashTempMapper @Inject constructor(
    private val tempMapper: TempMapper
) {

    fun toMashTemp(mashTempResponse: MashTempResponse): MashTemp = with(mashTempResponse){
        MashTemp(
            temp = tempMapper.toTemp(temp),
            duration = duration
        )
    }

    fun toMashTemp(mashTempResponse: List<MashTempResponse>): List<MashTemp> = mashTempResponse.map(::toMashTemp)
}