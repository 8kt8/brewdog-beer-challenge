package com.example.api.rx

import com.example.api.rx.CoreSchedulers
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

open class TestSchedulers : CoreSchedulers {
	override val computation: Scheduler
		get() = Schedulers.trampoline()

	override val dbIO: Scheduler
		get() = Schedulers.trampoline()

	override val diskIO: Scheduler
		get() = Schedulers.trampoline()

	override val networkIO: Scheduler
		get() = Schedulers.trampoline()

	override val mainThread: Scheduler
		get() = Schedulers.trampoline()
}
