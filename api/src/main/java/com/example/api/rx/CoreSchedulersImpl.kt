package com.example.api.rx

import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
open class CoreSchedulersImpl
@Inject
constructor(@Named("computationScheduler") override val computation: Scheduler,
            @Named("dbScheduler") override val dbIO: Scheduler,
            @Named("diskIOScheduler") override val diskIO: Scheduler,
            @Named("networkScheduler") override val networkIO: Scheduler,
            @Named("mainThreadScheduler") override val mainThread: Scheduler) : CoreSchedulers