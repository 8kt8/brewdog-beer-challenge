package com.example.api.repository.dbMapper

import com.example.api.common.fastLazy
import com.example.api.model.domain.method.Method
import com.squareup.moshi.Moshi
import javax.inject.Inject

class MethodDBMapper @Inject constructor(moshi: Moshi) {

    private val methodAdapter by fastLazy { moshi.adapter(Method::class.java) }

    fun toMethodDB(method: Method?) = methodAdapter.toJson(method)

    fun toMethod(methodDB: String) = methodAdapter.fromJson(methodDB)
}