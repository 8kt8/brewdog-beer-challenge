package com.example.api.repository.interactor

import com.example.api.repository.BeerRepository
import javax.inject.Inject

class GetBeerInteractor @Inject constructor(
   private val beerRepository: BeerRepository
) {
    fun getAll() = beerRepository.getAll()
}