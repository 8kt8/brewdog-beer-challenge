package com.example.api.repository.interactor

import com.example.api.repository.BeerRepository
import javax.inject.Inject

class RefreshBeerInteractor @Inject constructor(
    private val beerRepository: BeerRepository
) {

    fun refresh() = beerRepository.refreshBeer()
}