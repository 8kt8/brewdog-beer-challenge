package com.example.api.repository.dbMapper

import com.example.api.model.domain.beer.Beer
import com.example.database.dao.BeerDB
import javax.inject.Inject

class BeerDBMapper @Inject constructor(
    private val methodDBMapper: MethodDBMapper,
    private val ingredientsDBMapper: IngredientsDBMapper
) {

    fun toBeerDB(beer: Beer): BeerDB = with(beer) {
        BeerDB(
            id = id,
            name = name,
            description = description,
            imageUrl = imageUrl,
            abv = abv,
            methodDB = methodDBMapper.toMethodDB(method),
            ingredientsDB = ingredientsDBMapper.toIngredientsDB(ingredients)
        )
    }

    fun toBeer(beerDB: BeerDB): Beer = with(beerDB) {
        Beer(
            id = id,
            name = name,
            description = description,
            imageUrl = imageUrl,
            abv = abv,
            method = methodDBMapper.toMethod(methodDB),
            ingredients = ingredientsDBMapper.toIngredients(ingredientsDB)
        )
    }

    fun toBeerDB(beer: List<Beer>): List<BeerDB> = beer.map(::toBeerDB)

    fun toBeer(beerDB: List<BeerDB>): List<Beer> = beerDB.map(::toBeer)
}