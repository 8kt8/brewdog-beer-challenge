package com.example.api.repository.dbMapper

import com.example.api.common.fastLazy
import com.example.api.model.domain.ingredients.Ingredients
import com.squareup.moshi.Moshi
import javax.inject.Inject

class IngredientsDBMapper @Inject constructor(moshi: Moshi) {

    private val methodAdapter by fastLazy { moshi.adapter(Ingredients::class.java) }

    fun toIngredientsDB(ingredients: Ingredients?) = methodAdapter.toJson(ingredients)

    fun toIngredients(methodDB: String) = methodAdapter.fromJson(methodDB)
}