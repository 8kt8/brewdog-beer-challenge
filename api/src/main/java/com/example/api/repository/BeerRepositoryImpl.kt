package com.example.api.repository

import com.example.api.PunkApi
import com.example.api.model.domain.beer.Beer
import com.example.api.model.domain.beer.BeerMapper
import com.example.api.repository.dbMapper.BeerDBMapper
import com.example.api.rx.CoreSchedulers
import com.example.database.dao.BeerDao
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BeerRepositoryImpl @Inject constructor(
    private val punkApi: PunkApi,
    private val beerDao: BeerDao,
    private val beerMapper: BeerMapper,
    private val beerDBMapper: BeerDBMapper,
    private val coreSchedulers: CoreSchedulers
): BeerRepository{

    override fun refreshBeer(): Completable = punkApi.getAll()
            .map(beerMapper::toBeer)
            .subscribeOn(coreSchedulers.networkIO)
            .map(beerDBMapper::toBeerDB)
            .flatMapCompletable(beerDao::insertAll)
            .subscribeOn(coreSchedulers.dbIO)

    override fun getAll(): Flowable<List<Beer>> = beerDao.all
        .map(beerDBMapper::toBeer)
        .subscribeOn(coreSchedulers.dbIO)

    override fun getById(id: Int): Flowable<Beer> = beerDao.getById(id)
        .map(beerDBMapper::toBeer)
        .subscribeOn(coreSchedulers.dbIO)

}