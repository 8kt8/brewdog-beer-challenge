package com.example.api.repository.interactor

import com.example.api.repository.BeerRepository
import javax.inject.Inject

class GetBeerByIdInteractor @Inject constructor(
   private val beerRepository: BeerRepository
) {
    fun getById(id: Int) = beerRepository.getById(id)
}