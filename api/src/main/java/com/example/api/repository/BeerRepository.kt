package com.example.api.repository

import com.example.api.model.domain.beer.Beer
import io.reactivex.Completable
import io.reactivex.Flowable

interface BeerRepository {
    fun refreshBeer(): Completable
    fun getAll(): Flowable<List<Beer>>
    fun getById(id: Int): Flowable<Beer>
}