package com.example.api.repository

import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    internal abstract fun bindBeerRepository(beerRepository: BeerRepositoryImpl): BeerRepository
}