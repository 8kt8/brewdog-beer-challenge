package com.example.api

import com.example.api.model.BeerResponse
import io.reactivex.Single
import retrofit2.http.GET

interface PunkApi {

    @GET("beers")
    fun getAll(): Single<List<BeerResponse>>
}