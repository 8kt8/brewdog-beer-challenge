package com.example.api

class ApiConfig {

    val apiUrl = "https://api.punkapi.com"
    val readTimeout = 60L
    val connectionTimeout = 60L
    private val version = "2"

    fun punkApiUrl() = "$apiUrl/v$version/"
}