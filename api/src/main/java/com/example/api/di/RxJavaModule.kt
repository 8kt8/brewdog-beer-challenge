package com.example.api.di

import com.example.api.rx.CoreSchedulers
import com.example.api.rx.CoreSchedulersImpl
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
object RxJavaModule {

    @JvmStatic
    @Provides
    @Named("dbScheduler")
    internal fun provideDbScheduler(): Scheduler {
        return Schedulers.single()
    }

    @JvmStatic
    @Provides
    @Named("networkScheduler")
    internal fun provideNetworkScheduler(): Scheduler {
        return Schedulers.io()
    }

    @JvmStatic
    @Provides
    @Named("computationScheduler")
    internal fun provideComputationScheduler(): Scheduler {
        return Schedulers.computation()
    }

    @JvmStatic
    @Provides
    @Named("diskIOScheduler")
    internal fun provideDiskIoScheduler(): Scheduler {
        return Schedulers.io()
    }

    @JvmStatic
    @Provides
    @Named("mainThreadScheduler")
    internal fun provideMainThreadScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @JvmStatic
    @Provides
    @Singleton
    internal fun provideCoreSchedulers(
        @Named("computationScheduler") computation: Scheduler,
        @Named("dbScheduler") dbIO: Scheduler,
        @Named("diskIOScheduler") diskIO: Scheduler,
        @Named("networkScheduler") networkIO: Scheduler,
        @Named("mainThreadScheduler") mainThread: Scheduler
    ): CoreSchedulers {
        return CoreSchedulersImpl(computation, dbIO, diskIO, networkIO, mainThread)
    }

}