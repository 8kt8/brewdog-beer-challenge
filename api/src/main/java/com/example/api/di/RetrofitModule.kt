package com.example.api.di

import com.example.api.ApiConfig
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class RetrofitModule {

    @Provides
    @Singleton
    internal fun provideMoshiConverterFactory(moshi: Moshi): MoshiConverterFactory =
        MoshiConverterFactory.create(moshi)

    @Provides
    @Singleton
    internal fun provideRxJava2CallAdapterFactory(): RxJava2CallAdapterFactory =
        RxJava2CallAdapterFactory.create()

    @Provides
    @Singleton
    internal fun provideConfig(): ApiConfig = ApiConfig()

    @Provides
    internal fun provideRetrofitBuilder(
        apiConfig: ApiConfig,
        httpClient: OkHttpClient,
        moshiConverterFactory: MoshiConverterFactory,
        rxJavaCallAdapterFactory: RxJava2CallAdapterFactory
    ): Retrofit.Builder =
        Retrofit.Builder()
            .baseUrl(apiConfig.apiUrl)
            .client(httpClient)
            .addConverterFactory(moshiConverterFactory)
            .addCallAdapterFactory(rxJavaCallAdapterFactory)
}

fun <T> Retrofit.Builder.createService(
    url: String,
    serviceClass: Class<T>
): T = this
    .baseUrl(url)
    .build()
    .create(serviceClass)
