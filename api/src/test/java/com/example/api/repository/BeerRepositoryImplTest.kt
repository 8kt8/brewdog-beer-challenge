package com.example.api.repository

import com.example.api.PunkApi
import com.example.api.common.fastLazy
import com.example.api.model.BeerResponse
import com.example.api.model.domain.beer.Beer
import com.example.api.model.domain.beer.BeerMapper
import com.example.api.repository.dbMapper.BeerDBMapper
import com.example.api.rx.TestSchedulers
import com.example.api.utils.testCompletableFor
import com.example.database.dao.BeerDB
import com.example.database.dao.BeerDao
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Test

class BeerRepositoryImplTest {

    private val punkApi: PunkApi = mockk()
    private val beerDao: BeerDao = mockk()
    private val beerMapper: BeerMapper = mockk()
    private val beerDBMapper: BeerDBMapper = mockk()
    private val coreSchedulers = TestSchedulers()

    private val sut: BeerRepository by fastLazy {
        BeerRepositoryImpl(
            punkApi,
            beerDao,
            beerMapper,
            beerDBMapper,
            coreSchedulers
        )
    }

    private val beer: Beer = mockk()
    private val listOfBeer = listOf(beer, beer)
    private val beerDB: BeerDB = mockk()
    private val listOfBeerDB = listOf(beerDB, beerDB)

    init {
        every { beerDBMapper.toBeerDB(listOfBeer) } returns listOfBeerDB
    }

    @Test
    fun refreshBeer() {
        val beerResponse: BeerResponse = mockk()
        val listOfBeerResponse = listOf(beerResponse, beerResponse)
        every { punkApi.getAll() } returns Single.just(listOfBeerResponse)
        every { beerMapper.toBeer(listOfBeerResponse) } returns listOfBeer

        every { beerDao.insertAll(listOfBeerDB) } returns Completable.complete()

        val testCompletable = testCompletableFor { beerDao.insertAll(listOfBeerDB) }
        sut.refreshBeer()
            .test()
            .assertComplete()
        testCompletable.assertSubscribed()
    }

    @Test
    fun getAll() {
        every { beerDao.all } returns Flowable.just(listOfBeerDB)
        every { beerDBMapper.toBeer(listOfBeerDB) } returns listOfBeer

        sut.getAll().test().assertValue(listOfBeer)
    }

    @Test
    fun getById() {
        every { beerDao.getById(1) } returns Flowable.just(beerDB)
        every { beerDBMapper.toBeer(beerDB) } returns beer

        sut.getById(1).test().assertValue(beer)
    }

}