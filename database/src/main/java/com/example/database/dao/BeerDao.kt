package com.example.database.dao

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
abstract class BeerDao {

    @get:Transaction
    @get:Query("SELECT * FROM beer")
    abstract val all: Flowable<List<BeerDB>>

    @Transaction
    @Query("SELECT * FROM beer WHERE beer.id = :id")
    abstract fun getById(id: Int): Flowable<BeerDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(beer: List<BeerDB>): Completable
}