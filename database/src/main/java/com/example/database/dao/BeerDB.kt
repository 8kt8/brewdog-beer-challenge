package com.example.database.dao

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "Beer"
)
data class BeerDB(
    @field:PrimaryKey val id: Int,
    val name: String,
    val description: String,
    val imageUrl: String,
    val abv: Double,
    val methodDB: String,
    val ingredientsDB: String
)