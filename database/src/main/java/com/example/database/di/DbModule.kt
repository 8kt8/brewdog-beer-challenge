package com.example.database.di

import android.app.Application
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.example.database.AppDatabase
import com.example.database.dao.BeerDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule(val application: Application) {

    @ApplicationContext
    @Provides
    fun provideApplicationContext(): Context = application.applicationContext

    @Provides
    @Singleton
    internal fun appDatabase(
        @ApplicationContext context: Context
    ): AppDatabase =
        AppDatabase.getInstance(
            context
        )

    @Provides
    @Singleton
    internal fun beerDao(db: AppDatabase): BeerDao = db.beerDao()
}
